function showSlimeCastle(){
    show_modal()
    var content = document.getElementById("modal-content")
    content.innerHTML = 
    `
    <div class="modal-page">
  <div class="modal-body">

    <h2 class="text-uppercase title">Slime Castle</h2>
    <p class="item-intro text-muted">A platformer game made with Unreal Engine 4.</p>
    
    <div class="container button-container">

      <a href="https://gitlab.com/StephanieCheng/SlimeCastle2GovernmentEdition" class="button">Gitlab Download</a>
      
    </div>
    
    <div class="embed-container">
      <iframe width="560" height="315" src="https://www.youtube.com/embed/-iJRaCxH-SA" frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen=""></iframe>
    </div>


    <h3 id="goal">Goal</h3>

    <p>This project started as a small idea of making a old-school feeling platformer with Unreal Engine, the goal was
      to create something in the style of Jak And Daxter in terms of platforming, attacks and level feel. Such a simple
      concept of having 2 cancelable attacks and jumps would be a small enough scope to make a base game thats playable
      and enjoyable very fast from which other ideas could be made and other mechanics could be added.</p>

    <p><br /></p>
    <ul class="photo-gallery ">


      <li><img src="subpages/ProjectPages/SlimeCastle/Screenshot 2020-11-12 01.31.28.png"
          alt="" />
        <p></p>
      </li>



      <li><img src="subpages/ProjectPages/SlimeCastle/Screenshot 2020-11-12 01.30.43.png"
          alt="" />
        <p></p>
      </li>



      <li><img src="subpages/ProjectPages/SlimeCastle/Screenshot 2020-11-12 01.23.05.png"
          alt="" />
        <p></p>
      </li>



      <li><img src="subpages/ProjectPages/SlimeCastle/Screenshot 2020-11-12 01.25.51.png"
          alt="" />
        <p></p>
      </li>



      <li><img src="subpages/ProjectPages/SlimeCastle/Screenshot 2020-11-12 01.34.13.png"
          alt="" />
        <p></p>
      </li>



      <li><img src="subpages/ProjectPages/SlimeCastle/Screenshot 2020-11-12 01.36.38.png"
          alt="" />
        <p></p>
      </li>


    </ul>
    <p><br /></p>

    <h3 id="development">Development</h3>

    <p>The game was made over a course of 2 months with a team of two. All the assets of the game were hand made for the
      purpose of this platformer game. From the models and animations to the textures, levels and code everything was
      made from scratch without any pre-made assets. The sounds used are royalty free.</p>

    <p>The combat, character and first level was done in three weeks, later on we’ve decided to expand to more levels
      and add a bit more mechanics.</p>

    <p>The levels are made of modular assets specifically made for the game, they include castle, vegetation and prop
      assets.</p>

    <p>The materials were constructed to get a flat look to better show off the textures along with controlling shadows
      and highlight tones.</p>

    <p>The game is currently unfinished, but features two fully playable levels and one prototype.</p>

    <h4 id="levels">Levels</h4>

    <p><br /></p>
    <ul class="photo-gallery ">


      <li><img src="subpages/ProjectPages/SlimeCastle/snip.png" alt="" />
        <p></p>
      </li>



      <li><img src="subpages/ProjectPages/SlimeCastle/dungen.png" alt="" />
        <p></p>
      </li>



      <li><img src="subpages/ProjectPages/SlimeCastle/desrts.png" alt="" />
        <p></p>
      </li>



      <li><img src="subpages/ProjectPages/SlimeCastle/dungeon.png" alt="" />
        <p></p>
      </li>


    </ul>
    <p><br /></p>

    <h4 id="some-assets">Assets</h4>

    <p><br /></p>
    <ul class="photo-gallery photo-gallery4">


      <li><img src="subpages/ProjectPages/SlimeCastle/animrig.png" alt="" />
        <p></p>
      </li>



      <li><img src="subpages/ProjectPages/SlimeCastle/amLLVEFGE.png" alt="" />
        <p></p>
      </li>



      <li><img src="subpages/ProjectPages/SlimeCastle/someassets.png" alt="" />
        <p></p>
      </li>



      <li><img src="subpages/ProjectPages/SlimeCastle/CART.png" alt="" />
        <p></p>
      </li>


    </ul>
    <p><br /></p>
  </div>
  `        
}