function showDollyProject(){
    show_modal()
    var content = document.getElementById("modal-content")
    content.innerHTML = 
    `
    <div id="page" class="modal-page">
      <div class="modal-body">
        <!-- Project Details Go Here -->
        <h2 class="text-uppercase">Third Person Tracking Camera</h2>
        <p class="item-intro text-muted">A path following in game camera.</p>
        <!-- <img class="img-fluid d-block mx-auto" src="/nyangireworks/assets/img/portfolio/02-tracking-camera-full.png" alt="tracking-camera-image"> -->
        <p>
        <div class="container button-container">
  
          <a href="http://u3d.as/2ssN" class="btn button btn-secondary text-uppercase js-scroll-trigger">Unity Asset page</a>
          <!-- <a href="#" class="button">Contact me</a> -->
  
          <a href="https://nyangire.gitlab.io/nyangireworks/assets/docs/ThirdPersonTrackingCamerav1.0Documentation.pdf"
            class="btn button btn-secondary text-uppercase js-scroll-trigger">Documentation</a>
          <!-- <a href="#" class="button">Contact me</a> -->
  
          <br />
        </div>
  
        <h2 id="about">About</h2>
  
        <p>Third Person Tracking camera is a camera system that follows objects in real-time constrained on a Bezier curve
          path.</p>
  
        <p><br /></p>
  
        <div class="embed-container">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/9_8xLNCfTqo" title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen=""></iframe>
        </div>
  
        <p><br /></p>
  
        <p>The camera can determine its position on the path in order for the object to best fit within the cameras
          frustum
          and user defined borders. It can have preset view angles on the Bezier path or follow the object freely.</p>
  
        <p>This asset was made with Unity to allow fixed angle and dolly type camera scenes during gameplay when the
          player
          has full control of the player avatar, the camera can be used exclusively or in conjunction with other third
          person cameras for special areas and scenes. The camera is fully automatic, requiring no input from the player.
        </p>
  
        <h3 id="camera-in-the-editor">Camera In the Editor</h3>
  
        <p><br /></p>
        <ul class="photo-gallery ">
  
  
          <li>
            <video autoplay="" loop="" muted="" playsinline="">
              <source src="resources/projects/thirdpersontrackingcamera/itDoesAngls.webm" type="video/webm" />
              <source src="resources/projects/thirdpersontrackingcamera/itDoesAngls.mp4" type="video/mp4" />
            </video>
          </li>
  
  
  
          <li>
            <video autoplay="" loop="" muted="" playsinline="">
              <source src="resources/projects/thirdpersontrackingcamera/cameraIsPerf.webm" type="video/webm" />
              <source src="resources/projects/thirdpersontrackingcamera/cameraIsPerf.mp4" type="video/mp4" />
            </video>
          </li>
  
  
        </ul>
        <p><br /></p>
  
        <p>This project was inspired by old school third person horror games where the camera would have certain fixed
          positions but also animate on specific paths in regards to the player character, allowing for a more cinematic
          framing and memorable sequences within the gameplay.</p>
  
        <h3 id="old-school-examples">Old School Examples</h3>
  
        <p><br /></p>
        <ul class="photo-gallery ">
  
  
          <li>
            <video autoplay="" loop="" muted="" playsinline="">
              <source src="resources/projects/thirdpersontrackingcamera/SHcam.webm" type="video/webm" />
              <source src="resources/projects/thirdpersontrackingcamera/SHcam.mp4" type="video/mp4" />
            </video>
          </li>
  
  
  
          <li>
            <video autoplay="" loop="" muted="" playsinline="">
              <source src="resources/projects/thirdpersontrackingcamera/SHcam2.webm" type="video/webm" />
              <source src="resources/projects/thirdpersontrackingcamera/SHcam2.mp4" type="video/mp4" />
            </video>
          </li>
  
  
  
          <li>
            <video autoplay="" loop="" muted="" playsinline="">
              <source src="resources/projects/thirdpersontrackingcamera/HGcam.webm" type="video/webm" />
              <source src="resources/projects/thirdpersontrackingcamera/HGcam.mp4" type="video/mp4" />
            </video>
          </li>
  
  
  
          <li>
            <video autoplay="" loop="" muted="" playsinline="">
              <source src="resources/projects/thirdpersontrackingcamera/IPcam.webm" type="video/webm" />
              <source src="resources/projects/thirdpersontrackingcamera/IPcam.mp4" type="video/mp4" />
            </video>
          </li>
  
  
        </ul>
        <p><br /></p>
  
        <p>The goal of this project is to make this camera asset be able to replicate and match such behavior, offer
          customization for various different behaviors, be very quick to setup for fast prototyping.</p>
  
        <p>The camera can be used standalone to create a full third person old-school fixed/dolly camera type game or can
          be
          used in conjunction with other camera scrips for specific gameplay moments and tight areas.</p>
  
        </p>
        
      </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
  `     
}