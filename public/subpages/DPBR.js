function showDPBR(){
    show_modal()
    var content = document.getElementById("modal-content")
    content.innerHTML = 
    `
    <div class="modal-page">
  <div class="modal-body">
    <p class="title">
      DPBR
    </p>

    <p class="item-intro text-muted">Deferred renderer with Physically based shading.</p>

    <div class="container button-container">

      <a href="https://gitlab.com/Dood/portfolio/-/tree/master/DPBR" class="button">Gitlab Download</a>
      <!-- <a href="#" class="button">Contact me</a> -->

    </div>

    <div class="paragraph-left">

      <h3 id="deferred-renderer">Deferred Renderer</h3>

      <p>The usual method of rendering objects on the screen is the forward rendering method, where for each object we
        add
        up each lights source influence, this has a computational complexity of O(number of objects * number of lights)
        i.e. the multiplication of the number of objects and light sources in the scene.</p>

      <p>In order to circumvent this inefficiency deferred rendering uses buffers and fills them with information about
        the scene. The buffers store information such as coordinates, normals, albedo color and other material
        information.</p>

      <p>Then a buffer is used for the shading of the scene where each light uses the information of the previously
        constructed buffers to calculate its contribution and that contribution is added to the buffer, which then gets
        rendered out as the final scene. This has a computational complexity of O(screen resolution * number of lights).
        Resolution being fixed means that the computational complexity no longer depends on the complexity of the scene.
        However for the buffers which are the size of the resolution to be effectively manipulated this process requires
        a
        graphics card.</p>

      <p>There are plenty of benefits to both types of rendering, deferred is faster but forward is more compatible with
        older hardware. Both have exclusive features not available to the other.</p>

      <h3 id="physically-based-renderer---pbr">Physically Based Renderer - PBR</h3>

      <p>PBR is a type of rendering that shades the scene using physical simulations of light in order to achieve a more
        realistic look in 3D graphics.</p>

      <p>Physically based shading has replaced older methods that would use simple light intensity and reflection
        equations to color a fragment, and has replaced it with formulas simulating the real world interaction of light
        and <strong>micro-surfaces</strong>, <strong>energy conservation principles</strong> and <strong>the rendering
          equation</strong>.</p>

      <p>Micro-surfaces refer to the real world microscopic landscapes of different materials, the chaoticness of these
        surfaces determine how the light is being reflected and how material appears on a macro scale, with very smooth
        surfaces equating to a mirror polish and very rough surfaces diffusing the light in different directions.</p>

      <p>As the light ray hits the surface it splits into reflected and refracted ray, the energy of these rays equals
        the
        energy of the original ray, the refracted ray can also be absorbed by the material.</p>

      <p>Metalness is a special property because all metals absorb refactored rays and therefore never show a diffuse
        color.</p>

      <p>The rendering equation tries to describe how a “unit” of light is obtained given all the incoming light that
        interacts with a specific point of a given scene.</p>

      <p>It describes the sum of all incoming light rays from a half-sphere and combining on a point and then reflects
        that sum into the eye of the viewer.</p>

      <p>In computer graphics the viewers eye is a simple point in space acting as the camera, so we only need to
        consider
        light that reflects into that point, an easier way of doing that is to reverse the equation and cast rays from
        the
        camera onto all fragments of the scene.</p>

      <p>Physically based rendering in computer graphics is based on approximation of real world light physics formulas.
        However there’s more then one way to approximate and implement each of the needed formulas for physically based
        shading.</p>

      <p>This project has opted to use the following:</p>

      <ul>
        <li>Trowbridge-Reitz GGX</li>
        <li>Smith’s Schlick-GGX</li>
        <li>Fresnel-Schlick</li>
      </ul>

      <h3 id="the-project">The Project</h3>

      <p><br /></p>
      <ul class="photo-gallery ">


        <li><img src="https://imgur.com/bmiG8WG.png" alt="" />
          <p></p>
        </li>


      </ul>
      <p><br /></p>

      <p>The scene was rendered using OpenGL. Glew and Glsl were libraries used to write OpenGL commands in C++.</p>

      <p>Once the model and camera position and transform matricas have been calculated for the render, the vertex and
        fragment shader would be called for each model in the scene.</p>

      <p>In the render loop we render the models into the gbuffer and use that information to render each light into the
        final scene.</p>

      <div class="language-plaintext highlighter-rouge">
        <div class="highlight">
          <pre class="highlight"><code>renderer.StartGeometryPass();
renderer.Clear();
renderer.RenderModel(&amp;model);
renderer.RenderModel(&amp;cube2);
renderer.StartLightPass();
renderer.Clear();
renderer.RenderLight(&amp;ambient);
renderer.RenderLight(&amp;light1);
renderer.RenderLight(&amp;light2);
renderer.RenderLight(&amp;light3);
renderer.RenderSkybox(&amp;sky);
</code></pre>
        </div>
      </div>

      <p>The first vertex shader pass would finalize the positions of the vertices on the screen.</p>

      <p>The fragment shader would be used to ppulate the GBuffer with information on the object. For each fragment the
        sader would store that fragments position, color, normal and material data in a vector, creating a bunch of
        resolution sized textures that would contain non-color data for each fragment.</p>

      <p>The position, normal, color and material once extracted get written into the gBuffer for each fragment of the
        scene.</p>

      <div class="language-plaintext highlighter-rouge">
        <div class="highlight">
          <pre class="highlight"><code>gPosition = position;
gNormal = normal;
gColor = texture(diffuseTex, uv);
gMaterialProperties = texture(metallicRoughnessAOTex, uv)
</code></pre>
        </div>
      </div>

      <p>Once the GBuffer was populated a second pass through the shaders was necessary for each light and one for the
        ambient light and skybox.</p>

      <p>The vertex shader passes the data, while the fragment shader is where the physically based shading formulas
        come
        into effect to shade the fragment, and said fragment shading gets added onto the final buffer.</p>

      <p>Once all light data is processed the final image is rendered onto the screen.</p>



    </div>
  </div>
</div>
  `        
}