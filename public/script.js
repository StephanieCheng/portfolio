var modal_opacity = 0
var image_modal_opacity = 0
var modal_fade_speed = 0.2

function show_modal() {
    disable_main_scroll()
    var modal = document.getElementById("modal")
    modal.style.opacity = 0
    modal.style.visibility = "visible"

    modal_opacity = 0
    fadeIn = setInterval(function() {
        if (modal_opacity <= 1.1) {
            modal_opacity += modal_fade_speed
            modal.style.opacity = modal_opacity
        }
        else {
            clearInterval(fadeIn)
            modal.style.opacity = 1
            update_gallery_scale()
        }
    }, 10)

    var content = document.getElementById("modal-content")
    content.innerHTML = ""
}

function show_image(path){
    disable_main_scroll()
    var modal = document.getElementById("image-modal")
    modal.style.opacity = 0
    modal.style.visibility = "visible"

    image_modal_opacity = 0
    fadeIn = setInterval(function() {
        if (image_modal_opacity <= 1.1) {
            image_modal_opacity += modal_fade_speed
            modal.style.opacity = image_modal_opacity
        }
        else {
            clearInterval(fadeIn)
            modal.style.opacity = 1
        }
    }, 10)


    var content = document.getElementById("image-modal-content")
    content.innerHTML = ""

    var image = document.createElement("div") 
    image.classList.add("modal-image")
    content.appendChild(image);
    var img = document.createElement("img")
    img.src = path
    image.appendChild(img);

    image.onclick = hide_image_modal
    image.style.cursor = "pointer"
}

function hide_modal(){
    var modal = document.getElementById("modal")

    var fadeOut = setInterval(function() {
        if (modal_opacity >= 0) {
            modal_opacity -= modal_fade_speed
            modal.style.opacity = modal_opacity
        } else {
            clearInterval(fadeOut)
            modal.style.opacity = 0
            modal.style.visibility = "collapse"
            
            if (!is_any_modal_open()) {
                enable_main_scroll()
            }

            var content = document.getElementById("modal-content")
            content.innerHTML = ""
            modal_opacity = 0
        }
    }, 10)
}

function hide_image_modal(){
    var modal = document.getElementById("image-modal")

    var fadeOut = setInterval(function() {
        if (image_modal_opacity >= 0) {
            image_modal_opacity -= modal_fade_speed
            modal.style.opacity = image_modal_opacity
        } else {
            clearInterval(fadeOut)
            modal.style.opacity = 0
            modal.style.visibility = "collapse"
            
            if (!is_any_modal_open()) {
                enable_main_scroll()
            }
            
            var content = document.getElementById("image-modal-content")
            content.innerHTML = ""
            image_modal_opacity = 0
        }
    }, 10)
}

function is_image_modal_open() {
    var modal = document.getElementById("image-modal")
    return modal.style.opacity > 0.2
}

function is_modal_open() {
    var modal = document.getElementById("modal")
    return modal.style.opacity > 0.2
}

function is_any_modal_open() {
    return is_image_modal_open() || is_modal_open()
}

function enable_main_scroll() {
    document.body.style.overflowY = "auto"
}

function disable_main_scroll() {
    document.body.style.overflowY = "hidden"
}

window.onkeydown = function( event ) {
    if ( event.keyCode == 27 ) {
        if (is_image_modal_open()) {
            hide_image_modal()
            return 
        }
        if (is_modal_open()) {
            hide_modal()
            return
        }
        
    }
};

function update_gallery_scale() {
    var gap = 8
    
    var gallery = document.getElementsByClassName("modal-page")
    if (gallery.length <= 0)
    {
        return
    }

    var rows = document.getElementsByClassName("gallery-row")
    for (let i = 0; i < rows.length; i++)
    {
        var rowWidth = 0
        var images = rows[i].children

        var galleryWidth = gallery[0].clientWidth
        galleryWidth *= (2 / 3)
        galleryWidth -= (images.length * gap)

        for (let j = 0; j < images.length; j++)
        {
            var image = images[j].children[0]
            var imageScale = image.naturalHeight / 200
            image.style.transition = "0s"
            image.style.height = "200px"
            image.style.width = (image.naturalWidth / imageScale) + "px"
            rowWidth += image.naturalWidth / imageScale
        }

        var rowScale = rowWidth / galleryWidth

        for (let j = 0; j < images.length; j++)
        {
            var image = images[j].children[0]
            image.style.height = (parseFloat(image.style.height) / rowScale) + "px"
            image.style.width = (parseFloat(image.style.width) / rowScale) + "px"
        }   
    }

    setTimeout(function() {
        var rows = document.getElementsByClassName("gallery-row")
        for (let i = 0; i < rows.length; i++)
        {
            var images = rows[i].children
            for (let j = 0; j < images.length; j++)
            {
                var image = images[j].children[0]
                image.style.transition = "0.5s"
            }
        }
      }, 100);
}

window.onresize = update_gallery_scale;